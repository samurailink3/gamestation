# GameStation

GameStation is a combination of
Libretro/Retroarch/EmulationStation/RetroPie/Syncthing. In essence, it's a great
way to get all of your emulators up and running on a Debian Linux system super
easy. The default install is just a `.deb` file, but if you'd like, you can use
this project to build your own from source, getting the latest and greatest
emulators available.

Currently Debian Jessie and later (testing/sid) is supported. Ubuntu, Mint, and
others in the Debian family will probably work, but I haven't personally tested
them. Try it out, and let me know!

Combining this with Syncthing allows you to create multiple GameStations that
all have their save games, roms, and configurations synced across all your
devices.

One platform, open source, syncronized, and ready to play when you are.

## Install

Head over to [my GameStation project
post](http://samurailink3.com/blog/2015/10/04/gamestation/) for the Debian package to
install. It should be a double-click install on most Debian Linux systems. This
page will also take you through how to configure Syncthing to keep your
roms/saves in sync across devices. Gamestation expects roms to be in `~/roms/` with
this folder structure:

```
amiga
amstradcpc
apple2
atari2600
atari5200
atari7800
atari800
atarilynx
atarist
c64
coco
dragon32
fba
fds
gamegear
gb
gba
gbc
intellivision
macintosh
mame-advmame
mame-mame4all
mastersystem
megadrive
msx
n64
nds
neogeo
nes
ngp
pc
pcengine
ports
psx
quake3
scummvm
sega32x
segacd
sg-1000
snes
vectrex
videopac
wonderswan
zmachine
zxspectrum
```

And a symlink from `~/roms/megadrive` to `~/roms/genesis`: `ln -s ~/roms/megadrive ~/roms/genesis`

To run the system, use the command `emulationstation`.

## Build

If you'd like to build your own Debian package from source and get the latest
and greatest software, I've bundled the build proceedure into a few shell
scripts:

1. `./getdeps.sh` - This script pulls all of the base Debian dependencies
through `apt-get`.
2. `./fetch.sh` - This script clones the `libretro-super` and `EmulationStation`
git repositories to your local system.
3. `./build.sh` - This script will take a few hours to complete. It builds all
of the emulator libraries, retroarch, and emulationstation from source and
'installs' them into the temporary build directory.
4. `./install.sh` - This script builds a Debian package out of the temporary
build directory and installs it.

## Todo

There's lots of work to be done on this project, and all merge requests are
welcome. The short list includes:

1. Windows support - This will make it easy for non-techies to get their
emulators up and running easily.
2. RetroPie currently makes controller bindings between EmulationStation and
Retroarch seamless (for the most part). I'd love to port that over.
3. Create a build-bot system that puts out weekly beta builds of GameStation.
Ideally, this would be an ansible playbook or something equally as automated.
4. Automated Syncthing setup. Currently, Syncthing is pretty geared towards
techies. If mass adoption is going to be possible, this part will need to be
easier to set up. Maybe a web service that links IDs together through the
Syncthing API? Lots to consider.
5. **PIPE DREAM** - I'd LOVE to get this working on a
[PiGRRL](https://learn.adafruit.com/pocket-pigrrl) with wifi. Take your roms and
saves ANYWHERE! Sync when you hit wifi.

## License

Anything I have built on my own (including the bash scripts, Debian control
file, and/or documentation) is licensed under the GNU GPL v3.

## Credits

These amazing teams/people deserve the thanks, I just took what they did and
remixed it. This project is heavily inspired by and copied from:

### [RetroPie](https://github.com/RetroPie/RetroPie-Setup/)

Licensed under the [GNU GPL v3](https://www.gnu.org/licenses/gpl.html).

GameStation uses the RetroPie EmulationStation themes and some chuncks of EmulationStation configuration data to make things pretty.

### [Libretro](https://github.com/libretro) and [Retroarch](https://github.com/libretro/RetroArch)

The Libretro projects seem to be covered under multiple licenses. Check out their source code for more information. Retroarch in particular is licensed under the [GNU GPL v3](https://www.gnu.org/licenses/gpl.html).

GameStation does not exist without the amazing work that the Libretro
contributors have put toether (and this includes Retroarch). From the emulator
frontend, to the cores, to the settings, to the way the build system fits
together, they deserve the utmost respect and praise.

### [EmulationStation](https://github.com/Aloshi/EmulationStation)

Licensed under the [MIT License](http://opensource.org/licenses/MIT).

The amazing emulator frontend. Themeable, configurable, scriptable. Technically
GameStation could work without EmulationStation, but it would be a major pain to
use. Thanks, [Aloshi](https://github.com/Aloshi) for the awesome project!

### [Syncthing](https://syncthing.net/)

Licensed under the [MPLv2](http://opensource.org/licenses/MPL-2.0)

Syncthing allows GameStation systems to create and utilize a mesh network for
synchronizing roms, saves, and configuration files.
