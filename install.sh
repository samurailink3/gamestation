#!/bin/bash

TIMESTAMP=`date -u '+%Y%m%d%H%M%S'`

echo "Added date-version to debian package control file..."
sed "s/Version:.*/Version: $TIMESTAMP/" -i deb/DEBIAN/control

echo "Changing permissions to root for building package"
sudo chown -R root:root ./deb

echo "Building the debian package (this will take a while)..."
dpkg -b ./deb ./gamestation-$TIMESTAMP.deb

echo "Installing package"
sudo dpkg -i ./gamestation-$TIMESTAMP.deb

echo "Changing permissions back to normal"
sudo chown -R `whoami`:`whoami` ./deb

echo "Creating roms directory"
mkdir -p ~/roms/amiga
mkdir -p ~/roms/amstradcpc
mkdir -p ~/roms/apple2
mkdir -p ~/roms/atari2600
mkdir -p ~/roms/atari5200
mkdir -p ~/roms/atari7800
mkdir -p ~/roms/atari800
mkdir -p ~/roms/atarilynx
mkdir -p ~/roms/atarist
mkdir -p ~/roms/c64
mkdir -p ~/roms/coco
mkdir -p ~/roms/dragon32
mkdir -p ~/roms/fba
mkdir -p ~/roms/fds
mkdir -p ~/roms/gamegear
mkdir -p ~/roms/gb
mkdir -p ~/roms/gba
mkdir -p ~/roms/gbc
if [ ! -d ~/roms/genesis ]; then
  ln -s ~/roms/megadrive ~/roms/genesis
fi
mkdir -p ~/roms/intellivision
mkdir -p ~/roms/macintosh
mkdir -p ~/roms/mame-advmame
mkdir -p ~/roms/mame-mame4all
mkdir -p ~/roms/mastersystem
mkdir -p ~/roms/megadrive
mkdir -p ~/roms/msx
mkdir -p ~/roms/n64
mkdir -p ~/roms/nds
mkdir -p ~/roms/neogeo
mkdir -p ~/roms/nes
mkdir -p ~/roms/ngp
mkdir -p ~/roms/pc
mkdir -p ~/roms/pcengine
mkdir -p ~/roms/ports
mkdir -p ~/roms/psx
mkdir -p ~/roms/quake3
mkdir -p ~/roms/scummvm
mkdir -p ~/roms/sega32x
mkdir -p ~/roms/segacd
mkdir -p ~/roms/sg-1000
mkdir -p ~/roms/snes
mkdir -p ~/roms/vectrex
mkdir -p ~/roms/videopac
mkdir -p ~/roms/wonderswan
mkdir -p ~/roms/zmachine
mkdir -p ~/roms/zxspectrum

