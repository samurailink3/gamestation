#!/bin/bash

if [ ! -d "EmulationStation" ]; then
  echo "You should run './fetch.sh' first."
  exit
fi

if [ ! -d "libretro-super" ]; then
  echo "You should run './fetch.sh' first."
  exit
fi

echo "This build will take hours to complete."
echo "If you want to exit, CTRL+C now (you have 5 seconds)."
sleep 5

echo "Updating EmulationStation Repo"
cd EmulationStation
git fetch
git pull origin master

echo "Building EmulationStation"
cmake .
make
mkdir -p ../deb/usr/local/bin/
cp -f emulationstation ../deb/usr/local/bin/emulationstation
cd ..
echo "EmulationStation Built"

echo "Updating Libretro Repo"
cd libretro-super
git fetch
git pull origin master

echo "Building Libretro Cores"
make
cd ..
echo "Libretro Cores Built"

echo "Building Retroarch"
cd libretro-super
./retroarch-build.sh
cd retroarch
make install PREFIX=../../deb/usr/local
cd ../..
echo "Retroarch Built"

echo "Installing into work directory"
cd libretro-super
./libretro-install.sh ../deb/usr/local/lib/libretro
cd ..
echo "Install complete, please build and install Debian package"
