#!/bin/sh

# This script fetches the initial Git repos. It does not
# update the existing repos. You'll need to manually `git
# fetch` and `git pull origin master` to update the repos
# themselves. In the cast of `libretro-super`, this won't do
# much, you'll need to use `libretro-fetch.sh` to pull the
# individual component repos. Both `libretro-super` and the
# `EmulationStation` directories are ignored by
# `.gitignore`.

echo "Fetching build repos"
git clone https://github.com/libretro/libretro-super.git
git clone https://github.com/Herdinger/EmulationStation.git
echo "Done!"
